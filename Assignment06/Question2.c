/* This programme will print a fibonacci sequence upto the (N)th term using recursive functions */

#include <stdio.h>
int F(int);

int main()
{
	int a;
	int b=0;
	int c;
	
	printf("Enter the number: ");
	scanf("%d",&a);
	
	printf("Fibonacci sequence of the entered number will be :\n");
	
	for (c = 1; c <= a; c++)
	{
		printf("%d\n", F(b));
		b++;
	}
	
	return 0;
}

int F(int a)
{
	if (a == 0 || a ==1)
	   return a;
	else
	   return (F(a - 1) + F(a - 2));
}
