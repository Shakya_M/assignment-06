/* This programme will print a number triangle upto the (N)th term using recursive functions */

#include <stdio.h>
void pattern(int A);
void pattern(int A)
{
	if (A > 0)
	{
		pattern(A-1);
		while (A > 0){
			printf("%d",A);
			A=A-1;
			
		}
		printf("\n");
		
	}
}
int main()
{
	int A;
	
	printf("Enter any number: ");
	scanf("%d",&A);
	
	pattern(A);
	
	return 0;
}
